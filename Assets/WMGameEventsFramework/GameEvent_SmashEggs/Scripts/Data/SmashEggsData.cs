﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.GameEvent.SmashEggs
{
    [Serializable]
    public class SmashEggData
    {
        public string Item { get; set; }
        public int Id { get; set; }
        public bool IsTopPrize { get; set; }
        public List<SmashEggDataRate> SmashEggDataRates { get; set; } = new List<SmashEggDataRate>();
    }

    [Serializable]
    public class SmashEggDataRate
    {
        public int Quantity { get; set; }
        public float Rate { get; set; }
    }
}
