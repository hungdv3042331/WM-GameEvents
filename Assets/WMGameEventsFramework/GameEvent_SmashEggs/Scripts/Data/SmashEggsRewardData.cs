﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.GameEvent.SmashEggs
{
    public partial class SmashEggsRewardData
    {
        public int Coin;
        public int BoosterSkip;
        public int BoosterShield;
        public int BoosterMagnet;
    }
}