using System;
using System.Collections;
using System.Collections.Generic;
using WingsMob.GameEvent.SmashEggs;

namespace WingsMob.GameEvent.SmashEggs
{
    public static class GE_SmashEggDatabase
    {
        public static List<SmashEggData> SmashEggDatas = new List<SmashEggData>();
        public static List<SmashEggData> ItemTopPrizeDatas = new List<SmashEggData>();

        public static void InitSmashEggDatas(SmashEggConfigSO smashEggConfig)
        {
            foreach (var item in smashEggConfig.SmashEggDataConfigs)
            {
                SmashEggData data = new SmashEggData
                {
                    Item = item.ItemName,
                    Id = item.ItemId,
                    IsTopPrize = item.IsTopPrize,
                };
                foreach (var rate in item.SmashEggDataRates)
                {
                    SmashEggDataRate dataRate = new SmashEggDataRate
                    {
                        Quantity = rate.Quantity,
                        Rate = rate.Rate
                    };
                    data.SmashEggDataRates.Add(dataRate);
                }
                SmashEggDatas.Add(data);
                if (item.IsTopPrize)
                {
                    ItemTopPrizeDatas.Add(data);
                }
            }
        }

        public static SmashEggData GetSmashEggData(int id)
        {
            return SmashEggDatas.Find(item => item.Id == id);
        }

        public static SmashEggData GetSmashEggData(string itemName)
        {
            return SmashEggDatas.Find(item => item.Item == itemName);
        }
    }
}
