﻿using System;
using System.Collections.Generic;

namespace WingsMob.GameEvent.SmashEggs
{
    public interface IEventSmashEggsDelegate
    {
        void Initialize(List<IEggItem> listItems, List<ISmashEggTopPrizeItem> listTopPrizes, Action<float> startEvent, Action endEvent, float effectDuration);
        void OpenItemAction(IEggItem item, float duration);
        IEggItem GetCurrentItem();
    }
}
