﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.GameEvent.SmashEggs
{
    public interface ISmashEggTopPrizeItem
    {
        void SetItemId(int id);
        int GetItemId();
        void UnlockOneFragment();
        int GetFragmentAmount();
    }
}