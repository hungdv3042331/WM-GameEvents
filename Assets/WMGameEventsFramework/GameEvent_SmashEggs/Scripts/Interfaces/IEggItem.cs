﻿using UnityEngine.Events;

namespace WingsMob.GameEvent.SmashEggs
{
    public interface IEggItem
    {
        void SetItemClickEvent(UnityAction onclick);
        void SetInteractable(bool interactable);
        bool CanOpenItem(int currentResourceAmount, int requireResourceAmount);
    }
}