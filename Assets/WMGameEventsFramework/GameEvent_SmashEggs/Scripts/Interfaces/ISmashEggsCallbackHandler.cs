﻿using System;

namespace WingsMob.GameEvent.SmashEggs
{
    public interface ISmashEggsCallbackHandler
    {
        event Action<float> OnOpenStartEvent;
        event Action OnOpenEndEvent;
    }
}