﻿using System;

namespace WingsMob.GameEvent.SmashEggs
{
    [Serializable]
    public class TemplateData<T, U>
    {
        public T Key;
        public U Value;
    }
}