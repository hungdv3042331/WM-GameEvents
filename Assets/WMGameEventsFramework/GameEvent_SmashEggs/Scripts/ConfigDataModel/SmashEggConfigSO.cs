using UnityEngine;
using System;
using System.Collections.Generic;

namespace WingsMob.GameEvent.SmashEggs
{
    [CreateAssetMenu(fileName = "SmashEggData", menuName = "WingsMob/GameEvent/SmashEggs/Data", order = 1)]

    public class SmashEggConfigSO : ScriptableObject
    {
        [SerializeField] List<SmashEggDataConfig> m_listDatas = new List<SmashEggDataConfig>();
        public List<SmashEggDataConfig> SmashEggDataConfigs => m_listDatas;
    }


    [Serializable]
    public class SmashEggDataConfig
    {
        [SerializeField] string m_itemName;
        [SerializeField] int m_itemId;
        [SerializeField] List<SmashEggDataRateConfig> m_SmashEggDataRates;
        [SerializeField] bool m_isTopPrize;

        public string ItemName => m_itemName;
        public int ItemId => m_itemId;
        public List<SmashEggDataRateConfig> SmashEggDataRates => m_SmashEggDataRates;
        public bool IsTopPrize => m_isTopPrize;
    }

    [Serializable]
    public class SmashEggDataRateConfig
    {
        [SerializeField] int m_quantity;
        [SerializeField] float m_rate;

        public int Quantity => m_quantity;
        public float Rate => m_rate;
    }
}
