﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;

namespace WingsMob.GameEvent.SmashEggs
{
    public class GE_SmashEggsHandler : IEventSmashEggsDelegate, ISmashEggsCallbackHandler
    {
        public event Action<float> OnOpenStartEvent;
        public event Action OnOpenEndEvent;

        private IEggItem m_currentItem;
        private List<IEggItem> m_items;
        private List<ISmashEggTopPrizeItem> m_topPrizeItems;

        private float m_totalRate;
        private float m_effectDuration;
        private bool m_lockClick;
        private int m_clickCount;

        public void Initialize(List<IEggItem> listItems, List<ISmashEggTopPrizeItem> listTopPrizes, Action<float> openStartEvent, Action openEndEvent, float effectDuration)
        {
            m_items = listItems;
            m_topPrizeItems = listTopPrizes;
            OnOpenStartEvent += openStartEvent;
            OnOpenEndEvent += openEndEvent;
            m_effectDuration = effectDuration;
            m_lockClick = false;
            m_clickCount = 0;
            foreach (var item in m_items)
            {
                item.SetItemClickEvent(() =>
                {
                    if (m_lockClick == false)
                    {
                        OpenItemAction(item, m_effectDuration);
                    }
                });
            }

            m_totalRate = 0f;
            for (int i = 0; i < GE_SmashEggDatabase.SmashEggDatas.Count; i++)
            {
                var smashEggData = GE_SmashEggDatabase.GetSmashEggData(GE_SmashEggDatabase.SmashEggDatas[i].Id);
                foreach (var data in smashEggData.SmashEggDataRates)
                {
                    m_totalRate += data.Rate;
                }
            }
            for (int i = 0; i < m_topPrizeItems.Count; i++)
            {
                m_topPrizeItems[i].SetItemId(GE_SmashEggDatabase.ItemTopPrizeDatas[i].Id);
            }
        }

        public async void OpenItemAction(IEggItem item, float duration)
        {
            float randomRate = UnityEngine.Random.Range(0f, m_totalRate);
            float currentRate = 0f;
            for (int i = 0; i < GE_SmashEggDatabase.SmashEggDatas.Count; i++)
            {
                var smashEggData = GE_SmashEggDatabase.GetSmashEggData(GE_SmashEggDatabase.SmashEggDatas[i].Id);
                foreach (var data in smashEggData.SmashEggDataRates)
                {
                    currentRate += data.Rate;
                    if (currentRate >= randomRate)
                    {
                        var itemData = GE_SmashEggDatabase.GetSmashEggData(GE_SmashEggDatabase.SmashEggDatas[i].Id);
                        Common.Log($"You got x{data.Quantity} {itemData.Item} ");
                        if (itemData.IsTopPrize)
                        {
                            var index = m_topPrizeItems.FindIndex(x => x.GetItemId() == itemData.Id);
                            m_topPrizeItems[index].UnlockOneFragment();
                        }
                        break;
                    }
                }
                if (currentRate >= randomRate) break;
            }

            m_clickCount++;
            m_lockClick = true;
            m_currentItem = item;
            item.SetInteractable(false);
            OnOpenStartEvent?.Invoke(duration);
            await UniTask.WaitForSeconds(duration);
            OnOpenEndEvent?.Invoke();
            m_lockClick = false;
            if (m_clickCount == m_items.Count)
            {
                m_clickCount = 0;
                foreach (var element in m_items)
                {
                    element.SetInteractable(true);
                }
            }
        }

        public IEggItem GetCurrentItem()
        {
            return m_currentItem;
        }
    }
}