﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.GameEvent.SmashEggs
{
    public class GE_SmashEggsSystem
    {
        private GE_SmashEggsHandler m_handler;

        public GE_SmashEggsSystem(GE_SmashEggsHandler handler)
        {
            m_handler = handler;
        }

        public IEggItem GetCurrentItem()
        {
            return m_handler.GetCurrentItem();
        }

        public void Initialize(List<IEggItem> listItems, List<ISmashEggTopPrizeItem> listTopPrizes, Action<float> openStartEvent, Action openEndEvent, float effectDuration)
        {
            m_handler.Initialize(listItems, listTopPrizes, openStartEvent, openEndEvent, effectDuration);
        }

        public IEventSmashEggsDelegate GetEventSmashEggsDelegate()
        {
            return m_handler;
        }
    }
}