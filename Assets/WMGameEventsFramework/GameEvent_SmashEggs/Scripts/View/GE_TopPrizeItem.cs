﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace WingsMob.GameEvent.SmashEggs
{
    public class GE_TopPrizeItem : MonoBehaviour, ISmashEggTopPrizeItem
    {
        [SerializeField] Image[] m_imgFragements;
        [SerializeField] Button m_btnClaim;
        [SerializeField] Text m_textReceived;

        private List<int> m_listUnlockedFragments = new List<int>();
        private int m_itemId;

        private void Start()
        {
            m_btnClaim.onClick.AddListener(() => OnClaim());
            RefreshFragments();
        }

        private void OnClaim()
        {
            m_btnClaim.gameObject.SetActive(false);
            m_textReceived.gameObject.SetActive(true);
        }

        private void RefreshFragments()
        {
            m_listUnlockedFragments.Clear();
            foreach (var fragment in m_imgFragements)
            {
                fragment.gameObject.SetActive(true);
            }
        }

        public void SetItemId(int id)
        {
            m_itemId = id;
        }

        public int GetItemId()
        {
            return m_itemId;
        }

        public void UnlockOneFragment()
        {
            int randomIndex = UnityEngine.Random.Range(0, m_imgFragements.Length);
            while (m_listUnlockedFragments.Contains(randomIndex))
            {
                randomIndex = UnityEngine.Random.Range(0, m_imgFragements.Length);
            }
            m_listUnlockedFragments.Add(randomIndex);
            m_imgFragements[randomIndex].gameObject.SetActive(false);
            if (m_listUnlockedFragments.Count == m_imgFragements.Length)
            {
                m_btnClaim.gameObject.SetActive(true);
                m_textReceived.gameObject.SetActive(false);
                m_listUnlockedFragments.Clear();
            }
        }

        public int GetFragmentAmount()
        {
            return m_imgFragements.Length;
        }
    }
}