﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace WingsMob.GameEvent.SmashEggs
{
    public class GE_EggItem : MonoBehaviour, IEggItem
    {
        private Button m_itemButton;

        public void SetItemClickEvent(UnityAction callback)
        {
            m_itemButton = GetComponent<Button>();
            m_itemButton.onClick.AddListener(callback);
        }

        public void SetInteractable(bool interactable)
        {
            if (m_itemButton == null) m_itemButton = GetComponent<Button>();
            m_itemButton.interactable = interactable;
        }

        public bool CanOpenItem(int currentResourceAmount, int requireResourceAmount)
        {
            return currentResourceAmount >= requireResourceAmount;
        }
    }
}