using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace WingsMob.ScriptEditor
{
    public partial class GE_SmashEggsCreateObjectEditor : UnityEditor.Editor
    {

        #region GE_CreateObject
        [MenuItem("GameObject/Wingsmob/GameEvents/SmashEggs/SmashEggsPanel")]
        private static void CreateSmashEggsPanel()
        {

        }

        [MenuItem("GameObject/Wingsmob/GameEvents/SmashEggs/ItemTopPrize")]
        private static void CreateSmashEggs_ItemTopPrize()
        {

        }

        [MenuItem("GameObject/Wingsmob/GameEvents/SmashEggs/ItemEggs")]
        private static void CreateSmashEggs_ItemEggs()
        {
           
        }
        #endregion
    }
}