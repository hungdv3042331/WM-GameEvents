﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WingsMob.GameEvent.SmashEggs;

namespace WingsMob.GameEvent.SmashEggs.Example
{
    public class GE_ExampleSmashEgg : MonoBehaviour
    {
        GE_SmashEggsSystem m_smashEggsSystem;

        [Header("Item")]
        [SerializeField] List<GE_EggItem> m_items;
        [SerializeField] List<GE_TopPrizeItem> m_topPrizeItems;

        [Header("Effect")]
        [SerializeField] float m_effectDuration = 1f;

        [Header("Data")]
        [SerializeField] SmashEggConfigSO m_dataConfig;

        [Header("Button get hammer")]
        [SerializeField] Button m_btn3Hammers;
        [SerializeField] Button m_btn10Hammers;

        [SerializeField] int m_smashPrice = 1;

        private int HammerAmount
        {
            set { GE_ExampleSmashEggProfileData.SaveHammerAmount(value); }
            get { return GE_ExampleSmashEggProfileData.GetHammerAmount(); }
        }

        private void Start()
        {
            GE_SmashEggDatabase.InitSmashEggDatas(m_dataConfig);
            m_smashEggsSystem = new GE_SmashEggsSystem(new GE_SmashEggsHandler());
            List<IEggItem> items = new List<IEggItem>();
            foreach (var item in m_items)
            {
                items.Add(item);
            }
            List<ISmashEggTopPrizeItem> topPrizeItems = new List<ISmashEggTopPrizeItem>();
            foreach (var item in m_topPrizeItems)
            {
                topPrizeItems.Add(item);
            }
            m_smashEggsSystem.Initialize(items, topPrizeItems, OnOpenStartEvent, OnOpenEndEvent, m_effectDuration);
            HammerAmount = GE_ExampleSmashEggProfileData.GetHammerAmount();
            m_btn3Hammers.onClick.AddListener(() => OnGetHammer(3));
            m_btn10Hammers.onClick.AddListener(() => OnGetHammer(10));
        }

        private void OnOpenStartEvent(float duration)
        {
            HammerAmount -= m_smashPrice;
            Common.Log("Start current item animation");
        }

        private void OnOpenEndEvent()
        {
            Common.Log("End current item animation");
        }

        private void OnGetHammer(int amount)
        {
            Debug.Log($"You got {amount} hammers");
            HammerAmount += amount;
        }
    }
}