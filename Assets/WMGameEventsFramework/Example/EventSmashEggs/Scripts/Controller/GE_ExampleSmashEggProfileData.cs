﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WingsMob.GameEvent.SmashEggs.Example
{
    public class GE_ExampleSmashEggProfileData
    {
        public static void SaveHammerAmount(int amount)
        {
            PlayerPrefs.SetInt("HammerAmount", amount);
        }

        public static int GetHammerAmount()
        {
            return PlayerPrefs.GetInt("HammerAmount", 0);
        }
    }
}